package com.omsface

import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.ktor.application.call
import org.jetbrains.ktor.host.embeddedServer
import org.jetbrains.ktor.http.ContentType
import org.jetbrains.ktor.jetty.Jetty
import org.jetbrains.ktor.response.respondText
import org.jetbrains.ktor.routing.get
import org.jetbrains.ktor.routing.routing

fun main(args: Array<String>) {
    println("Hello, World")
    // https://github.com/Kotlin/ktor
    createTables()
    embeddedServer(Jetty, 8080) {
        routing {
            get("/") {
                call.respondText("Hello, world!", ContentType.Text.Html)
            }
        }

    }.start(wait = true)
}

object Users : Table() {
    val id = integer("id").autoIncrement().primaryKey()
    val name = varchar("name", length = 50)

}

// https://github.com/JetBrains/Exposed#sql-dsl-sample
fun createTables() {
    Database.connect("jdbc:h2:mem:test", driver = "org.h2.Driver")
    transaction {
        SchemaUtils.create(Users)
        Users.insert {
            it[name] = "User Name"
        }
        for(user in Users.selectAll()) {
            println("${user[Users.id]}, ${user[Users.name]}")
        }
    }
}

